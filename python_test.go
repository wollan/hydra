package main

import (
	"bytes"
	"strings"
	"testing"
)

func Test_python_writeOutput(t *testing.T) {
	buf := &bytes.Buffer{}
	sut := python{writer: buf}

	sut.writeOutput(standardSyntaxUnitTest())

	actual := strings.TrimSpace(buf.String())
	expected := trimLeftBlock(`
		from __future__ import annotations
		from dataclasses import dataclass
		from enum import Enum
		from typing import Optional, List, Dict, Any


		@dataclass(frozen=True)
		class Order:
			orderNo: str
			lines: List[OrderLine] #there must be at least one line
			vouchers: Dict[str, Voucher]
			planet: Planet

		
		# This is an order line
		@dataclass(frozen=True)
		class OrderLine:
			lineNo: int
			# Article number is not set initially
			articleNo: Optional[int]

		
		@dataclass(frozen=True)
		class Voucher:
			percent: float
	
		
		class Planet(int, Enum):
			mercury = 0
			venus = 1
			earth = 100
			mars = 101
			jupiter = 102
			saturn = 1000
			uranus = 1001
			neptune = 1002
		
		
		class CompassPoint(str, Enum):
			north = "north"
			south = "south"
			east = "east"
			west = "west"
		
		
		def dict_to_order(d: Dict[str, Any]) -> Order:
			return Order(
				orderNo=d['orderNo'],
				lines=[dict_to_orderline(a) for a in d['lines']],
				vouchers={a: dict_to_voucher(b) for a, b in d['vouchers'].items()},
				planet=Planet(d['planet']),
			)
		
		
		def dict_to_orderline(d: Dict[str, Any]) -> OrderLine:
			return OrderLine(
				lineNo=d['lineNo'],
				articleNo=d['articleNo'],
			)
		
		
		def dict_to_voucher(d: Dict[str, Any]) -> Voucher:
			return Voucher(
				percent=d['percent'],
			)
		`)
	assertEqual(t, expected, actual)
}

func Test_formatPythonType(t *testing.T) {
	tests := []struct {
		name      string
		hydraType hydraType
		want      string
	}{
		{
			name:      "array to list",
			hydraType: []string{"[", "Int32", "]"},
			want:      "List[int]",
		},
		{
			name:      "2D array to 2D list",
			hydraType: []string{"[", "[", "String", "]", "]"},
			want:      "List[List[str]]",
		},
		{
			name:      "option type",
			hydraType: []string{"Int64", "?"},
			want:      "Optional[int]",
		},
		{
			name:      "map type",
			hydraType: []string{"[", "Character", ":", "Double", "]"},
			want:      "Dict[str, float]",
		},
		{
			name:      "option type after map",
			hydraType: []string{"[", "Int8", ":", "Order", "?", "]", "?"},
			want:      "Optional[Dict[int, Optional[Order]]]",
		},
		{
			name:      "map with array",
			hydraType: []string{"[", "Bool", ":", "[", "Float", "]", "]"},
			want:      "Dict[bool, List[float]]",
		},
		{
			name:      "deep map-array combinations", // [Bool?: [[String : [Float]]]]
			hydraType: []string{"[", "Bool", "?", ":", "[", "[", "String", ":", "[", "Float", "]", "]", "]", "]"},
			want:      "Dict[Optional[bool], List[Dict[str, List[float]]]]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hydraType.validate(); err != nil {
				t.Errorf("precondition failed for test '%v': %v", tt.name, err)
			}
			if got := formatPythonType(tt.hydraType); got != tt.want {
				t.Errorf("formatPythonType() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_formatPythonDictExpr(t *testing.T) {
	defaultUserTypes := map[string]syntaxUnit{
		"Order":        startOfStruct{},
		"OrderLine":    startOfStruct{},
		"Planet":       integerEnumDecl{},
		"CompassPoint": stringEnumDecl{},
	}
	type args struct {
		letDecl   structLetDecl
		userTypes map[string]syntaxUnit
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "primitive",
			args: args{
				letDecl: structLetDecl{
					letName: "lineNo",
					letType: []string{"Int32"},
				},
				userTypes: defaultUserTypes,
			},
			want: "d['lineNo']",
		},
		{
			name: "another struct reference",
			args: args{
				letDecl: structLetDecl{
					letName: "line",
					letType: []string{"OrderLine"},
				},
				userTypes: defaultUserTypes,
			},
			want: "dict_to_orderline(d['line'])",
		},
		{
			name: "int-enum value",
			args: args{
				letDecl: structLetDecl{
					letName: "pl",
					letType: []string{"Planet"},
				},
				userTypes: defaultUserTypes,
			},
			want: "Planet(d['pl'])",
		},
		{
			name: "string-enum value",
			args: args{
				letDecl: structLetDecl{
					letName: "cp",
					letType: []string{"CompassPoint"},
				},
				userTypes: defaultUserTypes,
			},
			want: "CompassPoint(d['cp'])",
		},
		{
			name: "single array of struct refs",
			args: args{
				letDecl: structLetDecl{
					letName: "lines",
					letType: []string{"[", "OrderLine", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[dict_to_orderline(a) for a in d['lines']]",
		},
		{
			name: "single array of primitives",
			args: args{
				letDecl: structLetDecl{
					letName: "chars",
					letType: []string{"[", "Character", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[a for a in d['chars']]",
		},
		{
			name: "single array of enums",
			args: args{
				letDecl: structLetDecl{
					letName: "planets",
					letType: []string{"[", "Planet", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[Planet(a) for a in d['planets']]",
		},
		{
			name: "double array of struct refs",
			args: args{
				letDecl: structLetDecl{
					letName: "lineMatrix",
					letType: []string{"[", "[", "OrderLine", "]", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[[dict_to_orderline(b) for b in a] for a in d['lineMatrix']]",
		},
		{
			name: "triple array of struct refs",
			args: args{
				letDecl: structLetDecl{
					letName: "lineIn3D",
					letType: []string{"[", "[", "[", "OrderLine", "]", "]", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[[[dict_to_orderline(c) for c in b] for b in a] for a in d['lineIn3D']]",
		},
		{
			name: "frypple array of struct refs",
			args: args{
				letDecl: structLetDecl{
					letName: "lineIn4D",
					letType: []string{"[", "[", "[", "[", "OrderLine", "]", "]", "]", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[[[[dict_to_orderline(e) for e in c] for c in b] for b in a] for a in d['lineIn4D']]",
		},
		{
			name: "single dict of struct refs",
			args: args{
				letDecl: structLetDecl{
					letName: "lines",
					letType: []string{"[", "String", ":", "OrderLine", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "{a: dict_to_orderline(b) for a, b in d['lines'].items()}",
		},
		{
			name: "double dict of struct refs",
			args: args{
				letDecl: structLetDecl{
					letName: "lines",
					letType: []string{"[", "String", ":", "[", "String", ":", "OrderLine", "]", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "{a: {c: dict_to_orderline(e) for c, e in b.items()} for a, b in d['lines'].items()}",
		},
		{
			name: "dict of arrays",
			args: args{
				letDecl: structLetDecl{
					letName: "lines",
					letType: []string{"[", "String", ":", "[", "Bool", "]", "]"},
				},
				userTypes: defaultUserTypes,
			},
			want: "{a: [c for c in b] for a, b in d['lines'].items()}",
		},
		{
			name: "array of dicts",
			args: args{
				letDecl: structLetDecl{
					letName: "lines",
					letType: []string{"[", "[", "String", ":", "Int", "?", "]", "]", "?"},
				},
				userTypes: defaultUserTypes,
			},
			want: "[{b: c for b, c in a.items()} for a in d['lines']]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.args.letDecl.letType.validate(); err != nil {
				t.Errorf("precondition failed for test '%v': %v", tt.name, err)
			}
			if got := formatPythonDictExpr(tt.args.letDecl, tt.args.userTypes); got != tt.want {
				t.Errorf("formatPythonDictExpr() = %v, want %v", got, tt.want)
			}
		})
	}
}
