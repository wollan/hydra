package main

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

type swift struct {
	writer io.Writer
}

func (lang swift) writeOutput(sus []syntaxUnit) {

	panicWithError := func(err error, su syntaxUnit) {
		panic(fmt.Sprintf("error while writing '%v' as swift code: %v", su, err))
	}

	for i := 0; i < len(sus); i++ {
		su := sus[i]

		switch v := su.(type) {
		case startOfFile:
		case startOfStruct:
			if _, err := fmt.Fprint(lang.writer, swiftLinePrefix(sus[i-1]), swiftVisibility(v.visibility),
				"struct ", v.name, formatSwiftStructProtocols(v.protocols), " {"); err != nil {
				panicWithError(err, su)
			}
		case structLetDecl:
			if _, err := fmt.Fprint(lang.writer, swiftLinePrefix(sus[i-1]),
				"    ", swiftVisibility(v.visibility), "let ", v.letName, ": ", formatSwiftType(v.letType)); err != nil {
				panicWithError(err, su)
			}
		case endOfStruct:
			if _, err := fmt.Fprint(lang.writer, swiftLinePrefix(sus[i-1]), "}"); err != nil {
				panicWithError(err, su)
			}
		case singleLineComment:
			if _, err := fmt.Fprint(lang.writer, swiftLinePrefix(sus[i-1]), cntToSpaceIndent(v.indent), "//", v.comment); err != nil {
				panicWithError(err, su)
			}
		case endOfLineComment:
			if _, err := fmt.Fprint(lang.writer, " //", v.comment); err != nil {
				panicWithError(err, su)
			}
		case integerEnumDecl:
			if _, err := fmt.Fprint(lang.writer, swiftLinePrefix(sus[i-1]), swiftVisibility(v.visibility),
				"enum ", v.name, ": Int32", formatSwiftEnumProtocols(v.protocols), " {\n    ",
				"case ", formatSwiftIntegerEnumCases(v.cases), "\n}"); err != nil {
				panicWithError(err, su)
			}
		case stringEnumDecl:
			if _, err := fmt.Fprint(lang.writer, swiftLinePrefix(sus[i-1]), swiftVisibility(v.visibility),
				"enum ", v.name, ": String", formatSwiftEnumProtocols(v.protocols), " {\n    ",
				"case ", formatSwiftStringEnumCases(v.cases), "\n}"); err != nil {
				panicWithError(err, su)
			}
		default:
			panic(fmt.Sprintf("unknown syntax unit: %v", su))
		}
	}
}

func swiftLinePrefix(lastSU syntaxUnit) string {
	switch lastSU.(type) {
	case startOfFile:
		return "\n"
	case startOfStruct:
		return "\n"
	case structLetDecl:
		return "\n"
	case endOfStruct:
		return "\n\n"
	case singleLineComment:
		return "\n"
	case endOfLineComment:
		return "\n"
	case integerEnumDecl:
		return "\n\n"
	case stringEnumDecl:
		return "\n\n"
	default:
		panic(fmt.Sprintf("unknown syntax unit: %v", lastSU))
	}
}

func swiftVisibility(hydraVisibility string) string {
	switch hydraVisibility {
	case "public":
		return "public "
	case "internal":
		return ""
	default:
		panic(fmt.Sprint("invalid visibility: ", hydraVisibility))
	}
}

func formatSwiftStructProtocols(protocols []string) string {
	if len(protocols) == 0 {
		return ""
	}

	return ": " + strings.Join(protocols, ", ")
}

func formatSwiftEnumProtocols(protocols []string) string {
	if len(protocols) == 0 {
		return ""
	}

	return ", " + strings.Join(protocols, ", ")
}

func formatSwiftType(hydraType hydraType) string {
	builder := strings.Builder{}
	for _, v := range hydraType {
		builder.WriteString(v)
		if v == ":" {
			builder.WriteRune(' ')
		}
	}
	return builder.String()
}

func formatSwiftIntegerEnumCases(cases []integerEnumCase) string {
	singleLine := formatSwiftIntegerEnumCasesWithSep(cases, ", ")
	if len(singleLine)+9 <= wrapLineWidth {
		return singleLine
	}
	return formatSwiftIntegerEnumCasesWithSep(cases, ",\n         ")
}

func formatSwiftIntegerEnumCasesWithSep(cases []integerEnumCase, sep string) string {
	builder := strings.Builder{}
	for i, v := range cases {
		builder.WriteString(v.name)
		builder.WriteString(" = ")
		builder.WriteString(strconv.Itoa(int(v.value)))
		if i < len(cases)-1 {
			builder.WriteString(sep)
		}
	}
	return builder.String()
}

func formatSwiftStringEnumCases(cases []string) string {
	singleLine := strings.Join(cases, ", ")
	if len(singleLine)+9 <= wrapLineWidth {
		return singleLine
	}
	return strings.Join(cases, ",\n         ")
}
