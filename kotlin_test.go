package main

import (
	"bytes"
	"strings"
	"testing"
)

func Test_kotlin_writeOutput(t *testing.T) {
	buf := &bytes.Buffer{}
	sut := kotlin{writer: buf}

	sut.writeOutput(standardSyntaxUnitTest())

	actual := strings.TrimSpace(buf.String())
	expected := trimLeftBlock(`
		package mypackage
		
		@kotlinx.serialization.Serializable
		internal data class Order(
			internal val orderNo: String,
			internal val lines: List<OrderLine>, //there must be at least one line
			internal val vouchers: Map<Char, Voucher>,
			internal val planet: Planet
		)
		
		// This is an order line
		@kotlinx.serialization.Serializable
		data class OrderLine(
			val lineNo: Int,
			// Article number is not set initially
			val articleNo: Long?
		)
		
		@kotlinx.serialization.Serializable
		internal data class Voucher(
			internal val percent: Float
		)

		@kotlinx.serialization.Serializable(with = PlanetSerializer::class)
		internal enum class Planet(val value: Int) {
			mercury(0),
			venus(1),
			earth(100),
			mars(101),
			jupiter(102),
			saturn(1000),
			uranus(1001),
			neptune(1002)
        }
		
		enum class CompassPoint {
			north, south, east, west
		}
		
		private object PlanetSerializer : kotlinx.serialization.KSerializer<Planet> {
			private val valueMap = enumValues<Planet>().map { it.value to it }.toMap()
		
			override val descriptor = kotlinx.serialization.descriptors.PrimitiveSerialDescriptor(
				"Planet", kotlinx.serialization.descriptors.PrimitiveKind.INT)
		
			override fun serialize(encoder: kotlinx.serialization.encoding.Encoder, value: Planet) = 
				encoder.encodeInt(value.value)
		
			override fun deserialize(decoder: kotlinx.serialization.encoding.Decoder): Planet {
				val value = decoder.decodeInt()
				return valueMap[value] ?: 
					throw kotlinx.serialization.SerializationException("Planet does not contain element with value: $value")
			}
		}`)
	assertEqual(t, expected, actual)
}

func Test_formatKotlinType(t *testing.T) {
	tests := []struct {
		name      string
		hydraType hydraType
		want      string
	}{
		{
			name:      "array to list",
			hydraType: []string{"[", "Int32", "]"},
			want:      "List<Int>",
		},
		{
			name:      "2D array to 2D list",
			hydraType: []string{"[", "[", "String", "]", "]"},
			want:      "List<List<String>>",
		},
		{
			name:      "option type",
			hydraType: []string{"Int64", "?"},
			want:      "Long?",
		},
		{
			name:      "map type",
			hydraType: []string{"[", "Character", ":", "Double", "]"},
			want:      "Map<Char, Double>",
		},
		{
			name:      "option type after map",
			hydraType: []string{"[", "Int8", ":", "Order", "?", "]", "?"},
			want:      "Map<Byte, Order?>?",
		},
		{
			name:      "map with array",
			hydraType: []string{"[", "Bool", ":", "[", "Float", "]", "]"},
			want:      "Map<Boolean, List<Float>>",
		},
		{
			name:      "deep map-array combinations", // [Bool?: [[String : [Float]]]]
			hydraType: []string{"[", "Bool", "?", ":", "[", "[", "String", ":", "[", "Float", "]", "]", "]", "]"},
			want:      "Map<Boolean?, List<Map<String, List<Float>>>>",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hydraType.validate(); err != nil {
				t.Errorf("precondition failed for test '%v': %v", tt.name, err)
			}
			if got := formatKotlinType(tt.hydraType); got != tt.want {
				t.Errorf("formatKotlinType() = %v, want %v", got, tt.want)
			}
		})
	}
}
