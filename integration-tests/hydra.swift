
public struct Order: Codable {
    public let orderNo: String
    public let lines: [OrderLine]
    public let planet: Planet
    public let dir: CompassPoint
}

public struct OrderLine: Codable {
    public let lineNo: Int32
    public let articleNo: Int64?
}

public enum Planet: Int32, Codable {
    case mercury, venus, earth = 100, mars, jupiter, saturn = 1000, uranus, neptune
}

public enum CompassPoint: String, Codable {
    case north, south, east, west
}