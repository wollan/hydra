package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func main() {
	injson, err := ioutil.ReadFile("in.json")
	if err != nil {
		panic(err)
	}

	var order Order
	err = json.Unmarshal(injson, &order)
	if err != nil {
		panic(err)
	}

	outjson, err := json.Marshal(order)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(outjson))
}
