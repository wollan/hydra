from sut import dict_to_order
import json
from dataclasses import asdict

def read_json() -> str:
    with open('in.json', 'r') as file:
        return file.read()

injson = read_json()
order = dict_to_order(json.loads(injson))
outjson = json.dumps(asdict(order))
print(outjson)
