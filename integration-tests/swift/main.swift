import Foundation

func toJson<E : Encodable>(_ e: E) throws -> String {
    return String(bytes: try JSONEncoder().encode(e), encoding: .utf8)!
}

func fromJson<T : Decodable>(_ json: String) throws -> T {
    return try JSONDecoder().decode(T.self, from: json.data(using: .utf8)!)
}

let injson = try String(contentsOf: URL(fileURLWithPath: "in.json"), encoding: .utf8)
let order: Order = try fromJson(injson)
let outjson = try toJson(order)

print(outjson)
