import kotlinx.serialization.*
import kotlinx.serialization.json.*
import java.nio.file.Files
import java.nio.file.Paths

fun main() {
    val injson = Files.readString(Paths.get("in.json"))
    val order = Json.decodeFromString<Order>(injson)
    val outjson = Json.encodeToString(order)
    Files.writeString(Paths.get("out.json"), outjson)
}