package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	json1 := decode(os.Args[1])
	json2 := decode(os.Args[2])

	if json1 != json2 {
		panic(fmt.Sprintf("json not same\n\n%v\n\n%v\n\n", json1, json2))
	}
}

func decode(filename string) string {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	var j interface{}
	d := json.NewDecoder(file)
	if err := d.Decode(&j); err != nil {
		panic(err)
	}

	deleteNilFields(j)

	json, err := json.MarshalIndent(j, "", "    ")
	if err != nil {
		panic(err)
	}

	return string(json)
}

func deleteNilFields(j interface{}) {
	switch q := j.(type) {
	case map[string]interface{}:
		for k, v := range q {
			if v == nil {
				delete(q, k)
			} else {
				deleteNilFields(v)
			}
		}
	case []interface{}:
		for _, e := range q {
			deleteNilFields(e)
		}
	}
}
