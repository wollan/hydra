package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

const (
	wrapLineWidth = 80
)

func main() {
	langStr := flag.String("l", "swift", "the output language, one of: swift, kotlin, go, python")
	packageNameStr := flag.String("p", "", "override the default name of package/namespaces/module at top of output file")
	flag.Parse()

	lang := toLanguage(*langStr, os.Stdout)
	sus := readInputSource(os.Stdin, *packageNameStr)
	lang.writeOutput(sus)
}

func toLanguage(langStr string, writer io.Writer) language {
	switch langStr {
	case "swift":
		return swift{writer: writer}
	case "kotlin":
		return kotlin{writer: writer}
	case "go":
		return golang{writer: writer}
	case "python":
		return python{writer: writer}
	default:
		panic(fmt.Sprintf("invalid language: %v", langStr))
	}
}

type language interface {
	writeOutput(sus []syntaxUnit)
}

type syntaxUnit interface{}

type startOfFile struct {
	packageName string
}

type startOfStruct struct {
	visibility string // public/internal
	name       string
	protocols  []string
}

type structLetDecl struct {
	visibility string // public/internal
	letName    string
	letType    hydraType
}

type integerEnumDecl struct {
	visibility string // public/internal
	name       string
	protocols  []string
	cases      []integerEnumCase
}

type integerEnumCase struct {
	name  string
	value int32
}

type stringEnumDecl struct {
	visibility string // public/internal
	name       string
	protocols  []string
	cases      []string
}

type hydraType []string

func (ht hydraType) String() string {
	return strings.Join(ht, "")
}

type endOfStruct struct{}

type singleLineComment struct {
	indent  uint8
	comment string
}

type endOfLineComment struct {
	comment string
}

func readInputSource(reader io.Reader, packageName string) []syntaxUnit {
	if !isValidPackageName(packageName) {
		panic(fmt.Sprintf("invalid package name: %v", packageName))
	}

	tokenBuffer := []string{}
	tokenIndex := 0
	nextToken := func() *string {
		if tokenIndex < len(tokenBuffer) {
			t := tokenBuffer[tokenIndex]
			tokenIndex++
			return &t
		}

		return nil
	}

	scanner := bufio.NewScanner(reader)

	tokenStream := func() *string {
		if t := nextToken(); t != nil {
			return t
		}

		//make more tokens
		for {
			if scanner.Scan() {
				line := scanner.Text()
				tokenBuffer = tokenizeLine(line)
				tokenIndex = 0
				if t := nextToken(); t != nil {
					return t
				}
			} else {
				return nil
			}
		}
	}

	sus := processTokenStream(tokenStream, packageName)

	if err := scanner.Err(); err != nil {
		panic(fmt.Sprintf("error while reading source: %v", err))
	}

	return sus
}

func processTokenStream(tokenStream func() *string, packageName string) []syntaxUnit {
	const (
		rootCtx = iota
		startOfStructCtx
		insideStructCtx
		letDeclCtx
		typeCtx
		enumDeclCtx
	)

	ctxs := []int{rootCtx}
	ongoing := []string{}
	var emitAfterOngoingSU syntaxUnit = nil
	res := []syntaxUnit{}

	prependVisibilityToOngoing := func(firstToken string) {
		switch ongoing[0] {
		case "public":
		case firstToken:
			ongoing = append([]string{"internal"}, ongoing...)
		default:
			panic(fmt.Sprint("invalid ", firstToken, "-visibility for ", strings.Join(ongoing, " ")))
		}
	}
	getOngoingTokensAfter := func(a string) []string {
		res := []string{}
		isAfter := false
		for _, v := range ongoing {
			if isAfter {
				res = append(res, v)
			} else if v == a {
				isAfter = true
			}
		}
		return res
	}
	getOngoingTokensBetween := func(a, b string) []string {
		res := []string{}
		isBetween := false
		for _, v := range ongoing {
			if v == a {
				isBetween = true
			} else if v == b {
				isBetween = false
			} else if isBetween {
				res = append(res, v)
			}
		}
		return res
	}
	emit := func(su syntaxUnit) {
		res = append(res, su)
		ongoing = []string{}
		if emitAfterOngoingSU != nil {
			res = append(res, emitAfterOngoingSU)
			emitAfterOngoingSU = nil
		}
	}
	lastOrEmpty := func() string {
		if len(ongoing) == 0 {
			return ""
		}
		return ongoing[len(ongoing)-1]
	}
	emitStructLetDecl := func() {
		prependVisibilityToOngoing("let")
		emit(structLetDecl{
			visibility: ongoing[0],
			letName:    ongoing[2],
			letType:    hydraType(ongoing[4:]).validateOrPanic(),
		})
	}

	res = append(res, startOfFile{packageName: packageName})

	for {
		tokenPointer := tokenStream()
		if tokenPointer == nil {
			break
		}
		token := *tokenPointer

		// code comments
		if tt := strings.TrimSpace(token); strings.HasPrefix(tt, "//") {
			var su syntaxUnit
			if token[0] == '\n' {
				indentIdx := strings.Index(token, "//")
				su = singleLineComment{
					indent:  indentToCnt(token[1:indentIdx]),
					comment: token[indentIdx+2:],
				}
			} else {
				su = endOfLineComment{comment: token[2:]}
			}

			if len(ongoing) > 0 {
				emitAfterOngoingSU = su
			} else {
				res = append(res, su)
			}
			continue
		}

		ctx := ctxs[len(ctxs)-1]
		last := lastOrEmpty()

		switch ctx {
		case rootCtx:
			switch token {
			case "public":
				ongoing = append(ongoing, token)
			case "struct":
				ctxs = append(ctxs, startOfStructCtx)
				ongoing = append(ongoing, token)
			case "enum":
				ctxs = append(ctxs, enumDeclCtx)
				ongoing = append(ongoing, token)
			default:
				panic(fmt.Sprintf("invalid root token: %v", token))
			}
		case startOfStructCtx:
			switch token {
			case "{":
				prependVisibilityToOngoing("struct")
				emit(startOfStruct{
					visibility: ongoing[0],
					name:       ongoing[2],
					protocols:  getOngoingTokensAfter(":"),
				})
				ctxs = append(ctxs[:len(ctxs)-1], insideStructCtx)
			case ":":
				ongoing = append(ongoing, token)
			case ",":
			default:
				if isIdentifier(token) {
					ongoing = append(ongoing, token)
				} else {
					panic(fmt.Sprintf("invalid struct token: %v", token))
				}
			}
		case insideStructCtx:
			switch token {
			case "public":
				ongoing = append(ongoing, token)
			case "let":
				ctxs = append(ctxs, letDeclCtx)
				ongoing = append(ongoing, token)
			default:
				panic(fmt.Sprintf("invalid struct token: %v", token))
			}
		case letDeclCtx:
			switch last {
			case "let": // token is variable name
				ongoing = append(ongoing, token)
			case ":": // token is start of type
				ctxs = append(ctxs, typeCtx)
				ongoing = append(ongoing, token)
			default:
				switch token {
				case ":":
					ongoing = append(ongoing, token)
				default:
					panic(fmt.Sprintf("invalid let-declaration token: %v", token))
				}
			}
		case typeCtx:
			switch token {
			case "public":
				emitStructLetDecl()
				ctxs = ctxs[:len(ctxs)-2]
				ongoing = append(ongoing, token)
			case "let":
				emitStructLetDecl()
				ctxs = ctxs[:len(ctxs)-1]
				ongoing = append(ongoing, token)
			case "}":
				emitStructLetDecl()
				emit(endOfStruct{})
				ctxs = ctxs[:len(ctxs)-3]
			default: // random type definition token
				ongoing = append(ongoing, token)
			}
		case enumDeclCtx:
			switch token {
			case "}":
				prependVisibilityToOngoing("enum")
				protocols := filter(getOngoingTokensBetween(":", "{"), func(t string) bool {
					return t != ","
				})
				cases := splitOn(getOngoingTokensAfter("case"), ",")
				switch protocols[0] {
				case "Int32":
					emit(integerEnumDecl{
						visibility: ongoing[0],
						name:       ongoing[2],
						protocols:  protocols[1:],
						cases:      toIntegerEnumCases(cases),
					})
				case "String":
					emit(stringEnumDecl{
						visibility: ongoing[0],
						name:       ongoing[2],
						protocols:  protocols[1:],
						cases: mapSA(cases, func(c []string) string {
							if len(c) != 1 {
								panic(fmt.Sprintf("invalid string enum: %v", strings.Join(ongoing, " ")))
							}
							return c[0]
						}),
					})
				default:
					panic(fmt.Sprintf("invalid enum-declaration: %v", strings.Join(ongoing, " ")))
				}
				ctxs = ctxs[:len(ctxs)-1]
			default:
				ongoing = append(ongoing, token)
			}
		default:
			panic(fmt.Sprintf("bad ctx: %v", ctx))
		}
	}

	return res
}

func toIntegerEnumCases(cases [][]string) []integerEnumCase {
	enumValue := 0
	res := []integerEnumCase{}

	for _, c := range cases {
		if len(c) == 3 {
			i, err := strconv.Atoi(c[2])
			if err != nil {
				panic(fmt.Sprintf("invalid integer enum case conversion: %v", c))
			}
			enumValue = i
		} else if len(c) != 1 {
			panic(fmt.Sprintf("invalid integer enum case: %v", c))
		}

		res = append(res, integerEnumCase{name: c[0], value: int32(enumValue)})
		enumValue++
	}

	return res
}

func tokenizeLine(line string) []string {
	if tl := strings.TrimSpace(line); strings.HasPrefix(tl, "//") {
		return []string{"\n" + line}
	}

	res := []string{}
	ongoing := []rune{}

	for i, c := range line {
		if c == '/' && i+1 < len(line) && line[i+1] == '/' {
			return append(res, line[i:])
		}

		if validPartOfLagerToken(c) {
			if len(ongoing) > 0 {
				ongoing = append(ongoing, c) // continue larger token
			} else {
				ongoing = []rune{c} // start new larger token
			}
			continue
		}

		// not part of larger token -> first emit any ongoing larger one
		if len(ongoing) > 0 {
			res = append(res, string(ongoing))
			ongoing = []rune{}
		}

		if c != ' ' && c != '\t' {
			//..then emit this rune as single-rune-token
			res = append(res, string(c))
		}
	}

	if len(ongoing) > 0 {
		res = append(res, string(ongoing))
	}

	return res
}

func validPartOfLagerToken(c rune) bool {
	return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('0' <= c && c <= '9') || c == '_'
}

func isIdentifier(token string) bool {
	return len(token) != 0 && (len(token) > 1 || validPartOfLagerToken(rune(token[0])))
}

func isValidPackageName(packageName string) bool {
	if len(packageName) == 0 {
		return true
	}
	for _, v := range strings.Split(packageName, ".") {
		for _, r := range v {
			if !validPartOfLagerToken(r) {
				return false
			}
		}
	}
	return true
}

func (ht hydraType) validate() error {
	if len(ht) == 0 {
		return fmt.Errorf("found empty type")
	}
	hardBrackets := 0
	for _, v := range ht {
		switch len(v) {
		case 0:
			return fmt.Errorf("empty token in type: %v", ht)
		case 1:
			switch v {
			case "[":
				hardBrackets++
			case "]":
				hardBrackets--
			case ":", "?":
			default:
				if !validPartOfLagerToken(rune(v[0])) {
					return fmt.Errorf("invalid character '%v' in hydra type: %v", v[0], ht)
				}
			}
		default:
			for _, r := range v {
				if !validPartOfLagerToken(r) {
					return fmt.Errorf("invalid character '%v' in hydra type: %v", string(r), ht)
				}
			}
		}
	}
	if hardBrackets != 0 {
		return fmt.Errorf("bracket mismatch in type: %v", ht)
	}

	insideMapKey := false
	for i := len(ht) - 1; i >= 0; i-- {
		switch ht[i] {
		case ":":
			if insideMapKey {
				return fmt.Errorf("cannot use a map or array as a map key in: %v", ht)
			}
			insideMapKey = true
		case "[":
			insideMapKey = false
		case "]":
			if insideMapKey {
				return fmt.Errorf("cannot use a map or array as a map key in: %v", ht)
			}
		}
	}

	return nil
}

func (ht hydraType) validateOrPanic() hydraType {
	if err := ht.validate(); err != nil {
		panic(err)
	}
	return ht
}

func (ht hydraType) containsOptional() bool {
	return anyS(ht, "?")
}

func (ht hydraType) containsDictionary() bool {
	return anyS(ht, ":")
}

func (ht hydraType) containsList() bool {
	startBracketCnt := countOf(ht, "[")
	if startBracketCnt == 0 {
		return false
	}
	colonCnt := countOf(ht, ":")
	return startBracketCnt > colonCnt // equal count means only dicts
}

func allTypeDecls(sus []syntaxUnit) (res []hydraType) {
	for _, su := range sus {
		if letDecl, ok := su.(structLetDecl); ok {
			res = append(res, letDecl.letType)
		}
	}
	return
}

func mapS(vs []string, f func(string) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

func mapSA(vs [][]string, f func([]string) string) []string {
	vsm := make([]string, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}

func filter(ss []string, test func(string) bool) (res []string) {
	for _, s := range ss {
		if test(s) {
			res = append(res, s)
		}
	}
	return
}

func anyS(ss []string, s string) bool {
	for _, v := range ss {
		if v == s {
			return true
		}
	}
	return false
}

func anyHT(xs []hydraType, test func(hydraType) bool) bool {
	for _, x := range xs {
		if test(x) {
			return true
		}
	}
	return false
}

func countOf(ss []string, s string) int {
	cnt := 0
	for _, v := range ss {
		if v == s {
			cnt++
		}
	}
	return cnt
}

func (ht hydraType) extractMapOrArrayParts() (hydraType, hydraType, hydraType) {
	depth := 1
	p2startIndex := -1
	restStartIndex := -1
	p1 := []string{}
	p2 := []string{}
	for i := 1; i < len(ht); i++ {
		switch ht[i] {
		case "[":
			depth++
		case ":":
			if depth == 1 {
				p1 = ht[1:i]
				p2startIndex = i + 1
			}
		case "]":
			depth--
			if depth == 0 {
				if p2startIndex > 0 {
					p2 = ht[p2startIndex:i]
				} else {
					p1 = ht[1:i]
				}
				restStartIndex = i + 1
				break
			}
		}
	}

	if len(p1) == 0 || depth != 0 {
		panic(fmt.Sprintf("bad hydra type: %v", ht))
	}

	var rest []string
	if restStartIndex > 0 {
		rest = ht[restStartIndex:]
	} else {
		rest = []string{}
	}

	return p1, p2, rest
}

func indentToCnt(indent string) uint8 {
	var cnt uint8 = 0
	for _, v := range indent {
		switch v {
		case ' ':
			cnt++
		case '\t':
			cnt += 4
		default:
			panic(fmt.Sprintf("bad indent: %s", indent))
		}
	}
	return cnt
}

func cntToSpaceIndent(cnt uint8) string {
	return strings.Repeat(" ", int(cnt))
}

func cntToTabIndent(cnt uint8) string {
	return strings.Repeat("\t", int(cnt/4)) + cntToSpaceIndent(cnt%4)
}

func getCurrentStructSU(currIndex int, sus []syntaxUnit) startOfStruct {
	for i := currIndex - 1; i >= 0; i-- {
		if sos, ok := sus[i].(startOfStruct); ok {
			return sos
		}
	}

	panic(fmt.Sprintf("no containing struct in %+v", sus))
}

func getAllUserTypeVisibilities(sus []syntaxUnit) map[string]string {
	res := map[string]string{}
	for _, su := range sus {
		switch v := su.(type) {
		case startOfStruct:
			res[v.name] = v.visibility
		case integerEnumDecl:
			res[v.name] = v.visibility
		case stringEnumDecl:
			res[v.name] = v.visibility
		}
	}
	return res
}

func getUserTypesDict(sus []syntaxUnit) map[string]syntaxUnit {
	res := map[string]syntaxUnit{}
	for _, su := range sus {
		switch v := su.(type) {
		case startOfStruct:
			res[v.name] = v
		case integerEnumDecl:
			res[v.name] = v
		case stringEnumDecl:
			res[v.name] = v
		}
	}
	return res
}

func splitOn(tokens []string, sep string) [][]string {
	res := [][]string{}
	ongoing := []string{}
	for _, v := range tokens {
		if v == sep {
			if len(ongoing) > 0 {
				res = append(res, ongoing)
			}
			ongoing = []string{}
		} else {
			ongoing = append(ongoing, v)
		}
	}
	if len(ongoing) > 0 {
		res = append(res, ongoing)
	}
	return res
}

func (su startOfStruct) isCodable() bool {
	return anyS(su.protocols, "Codable")
}

func anyCodableStruct(sus []syntaxUnit) bool {
	for _, v := range sus {
		if sos, ok := v.(startOfStruct); ok {
			if sos.isCodable() {
				return true
			}
		}
	}

	return false
}
