package main

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

type kotlin struct {
	writer io.Writer
}

func (lang kotlin) writeOutput(sus []syntaxUnit) {

	panicWithError := func(err error, su syntaxUnit) {
		panic(fmt.Sprintf("error while writing '%v' as kotlin code: %v", su, err))
	}

	footers := []string{}

	for i := 0; i < len(sus); i++ {
		su := sus[i]

		switch v := su.(type) {
		case startOfFile:
			if len(v.packageName) > 0 {
				if _, err := fmt.Fprint(lang.writer, "package ", v.packageName); err != nil {
					panicWithError(err, su)
				}
			}
		case startOfStruct:
			if _, err := fmt.Fprint(lang.writer, kotlinLinePrefix(sus[i-1]), kotlinClassAnnotations(v.protocols),
				kotlinVisibility(v.visibility), "data class ", v.name, "("); err != nil {
				panicWithError(err, su)
			}
		case structLetDecl:
			if _, err := fmt.Fprint(lang.writer, kotlinLinePrefix(sus[i-1]),
				"    ", kotlinVisibility(v.visibility), "val ", v.letName, ": ", formatKotlinType(v.letType),
				kotlinLetDeclSuffix(sus, i)); err != nil {
				panicWithError(err, su)
			}
		case endOfStruct:
			if _, err := fmt.Fprint(lang.writer, kotlinLinePrefix(sus[i-1]), ")"); err != nil {
				panicWithError(err, su)
			}
		case singleLineComment:
			if _, err := fmt.Fprint(lang.writer, kotlinLinePrefix(sus[i-1]),
				cntToSpaceIndent(v.indent), "//", v.comment); err != nil {
				panicWithError(err, su)
			}
		case endOfLineComment:
			if _, err := fmt.Fprint(lang.writer, " //", v.comment); err != nil {
				panicWithError(err, su)
			}
		case integerEnumDecl:
			if _, err := fmt.Fprint(lang.writer, kotlinLinePrefix(sus[i-1]), kotlinIntegerEnumAnnotations(v.protocols, v.name),
				kotlinVisibility(v.visibility), "enum class ", v.name, "(val value: Int) {\n    ",
				formatKotlinIntegerEnumCases(v.cases), "\n}"); err != nil {
				panicWithError(err, su)
			}
			footers = append(footers, formatKotlinIntEnumSerializer(v.name))
		case stringEnumDecl:
			if _, err := fmt.Fprint(lang.writer, kotlinLinePrefix(sus[i-1]), kotlinVisibility(v.visibility),
				"enum class ", v.name, " {\n    ", formatKotlinStringEnumCases(v.cases), "\n}"); err != nil {
				panicWithError(err, su)
			}
		default:
			panic(fmt.Sprintf("unknown syntax unit: %v", su))
		}
	}

	for _, footer := range footers {
		if _, err := fmt.Fprint(lang.writer, footer); err != nil {
			panic(fmt.Sprintf("error while writing footer: %s", footer))
		}
	}
}

// a precondition is that hydra type is valid
func formatKotlinType(hydraType hydraType) string {

	if len(hydraType) > 0 && hydraType[0] == "[" {
		p1, p2, rest := hydraType.extractMapOrArrayParts()
		if len(p2) > 0 {
			return "Map<" + formatKotlinType(p1) + ", " + formatKotlinType(p2) + ">" + formatKotlinType(rest)
		}

		return "List<" + formatKotlinType(p1) + ">" + formatKotlinType(rest)
	}

	return strings.Join(mapS(hydraType, func(t string) string {
		switch t {
		case "Int8":
			return "Byte"
		case "Int16":
			return "Short"
		case "Int32":
			return "Int"
		case "Int64", "Int":
			return "Long"
		case "Character":
			return "Char"
		case "Bool":
			return "Boolean"
		case "[", ":", "]":
			panic(fmt.Sprintf("bad hydra type '%v' in %v", t, hydraType))
		default:
			return t
		}
	}), "")
}

func kotlinLinePrefix(lastSU syntaxUnit) string {
	switch lastSU.(type) {
	case startOfFile:
		return "\n\n"
	case startOfStruct:
		return "\n"
	case structLetDecl:
		return "\n"
	case endOfStruct:
		return "\n\n"
	case singleLineComment:
		return "\n"
	case endOfLineComment:
		return "\n"
	case integerEnumDecl:
		return "\n\n"
	case stringEnumDecl:
		return "\n\n"
	default:
		panic(fmt.Sprintf("unknown syntax unit: %v", lastSU))
	}
}

func kotlinLetDeclSuffix(sus []syntaxUnit, i int) string {
	for j := i + 1; j < len(sus); j++ {
		switch sus[j].(type) {
		case singleLineComment:
		case endOfLineComment:
		case structLetDecl:
			return ","
		default:
			return ""
		}
	}

	panic(fmt.Sprint("let decl in struct cannot be the last syntax unit"))
}

func kotlinVisibility(hydraVisibility string) string {
	switch hydraVisibility {
	case "public":
		return ""
	case "internal":
		return "internal "
	default:
		panic(fmt.Sprint("invalid visibility: ", hydraVisibility))
	}
}

func kotlinClassAnnotations(protocols []string) string {
	for _, p := range protocols {
		if p == "Codable" {
			return "@kotlinx.serialization.Serializable\n"
		}
	}

	return ""
}

func kotlinIntegerEnumAnnotations(protocols []string, enumName string) string {
	for _, p := range protocols {
		if p == "Codable" {
			return fmt.Sprintf("@kotlinx.serialization.Serializable(with = %sSerializer::class)\n", enumName)
		}
	}

	return ""
}

func formatKotlinIntegerEnumCases(cases []integerEnumCase) string {
	singleLine := formatKotlinIntegerEnumCasesWithSep(cases, ", ")
	if len(singleLine)+4 <= wrapLineWidth {
		return singleLine
	}
	return formatKotlinIntegerEnumCasesWithSep(cases, ",\n    ")
}

func formatKotlinIntegerEnumCasesWithSep(cases []integerEnumCase, sep string) string {
	builder := strings.Builder{}
	for i, v := range cases {
		builder.WriteString(v.name)
		builder.WriteRune('(')
		builder.WriteString(strconv.Itoa(int(v.value)))
		builder.WriteRune(')')
		if i < len(cases)-1 {
			builder.WriteString(sep)
		}
	}
	return builder.String()
}

func formatKotlinStringEnumCases(cases []string) string {
	singleLine := strings.Join(cases, ", ")
	if len(singleLine)+4 <= wrapLineWidth {
		return singleLine
	}
	return strings.Join(cases, ",\n    ")
}

func formatKotlinIntEnumSerializer(enumName string) string {
	return fmt.Sprintf(`

private object %[1]sSerializer : kotlinx.serialization.KSerializer<%[1]s> {
    private val valueMap = enumValues<%[1]s>().map { it.value to it }.toMap()

    override val descriptor = kotlinx.serialization.descriptors.PrimitiveSerialDescriptor(
        "%[1]s", kotlinx.serialization.descriptors.PrimitiveKind.INT)

    override fun serialize(encoder: kotlinx.serialization.encoding.Encoder, value: %[1]s) = 
        encoder.encodeInt(value.value)

    override fun deserialize(decoder: kotlinx.serialization.encoding.Decoder): %[1]s {
        val value = decoder.decodeInt()
        return valueMap[value] ?: 
            throw kotlinx.serialization.SerializationException("%[1]s does not contain element with value: $value")
    }
}`, enumName)
}
