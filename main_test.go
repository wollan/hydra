package main

import (
	"math"
	"reflect"
	"strings"
	"testing"
)

func Test_tokenizeLine(t *testing.T) {
	tests := []struct {
		input    string
		expected []string
	}{
		{
			input:    "struct OrderLine{let lineNo: Int32",
			expected: []string{"struct", "OrderLine", "{", "let", "lineNo", ":", "Int32"},
		},
		{
			input: "		struct OrderLine  {     ",
			expected: []string{"struct", "OrderLine", "{"},
		},
		{
			input:    "    // This is a line comment  ",
			expected: []string{"\n    // This is a line comment  "},
		},
		{
			input:    "struct Order { //customer order",
			expected: []string{"struct", "Order", "{", "//customer order"},
		},
	}

	for _, tc := range tests {
		actual := tokenizeLine(tc.input)
		assertEqual(t, tc.expected, actual)
	}
}

func standardSyntaxUnitTest() []syntaxUnit {
	return []syntaxUnit{
		startOfFile{packageName: "mypackage"},
		startOfStruct{visibility: "internal", name: "Order", protocols: []string{"Codable"}},
		structLetDecl{visibility: "internal", letName: "orderNo", letType: []string{"String"}},
		structLetDecl{visibility: "internal", letName: "lines", letType: []string{"[", "OrderLine", "]"}},
		endOfLineComment{comment: "there must be at least one line"},
		structLetDecl{visibility: "internal", letName: "vouchers", letType: []string{"[", "Character", ":", "Voucher", "]"}},
		structLetDecl{visibility: "internal", letName: "planet", letType: []string{"Planet"}},
		endOfStruct{},
		singleLineComment{indent: 0, comment: " This is an order line"},
		startOfStruct{visibility: "public", name: "OrderLine", protocols: []string{"Equatable", "Codable"}},
		structLetDecl{visibility: "public", letName: "lineNo", letType: []string{"Int32"}},
		singleLineComment{indent: 4, comment: " Article number is not set initially"},
		structLetDecl{visibility: "public", letName: "articleNo", letType: []string{"Int64", "?"}},
		endOfStruct{},
		startOfStruct{visibility: "internal", name: "Voucher", protocols: []string{"Codable"}},
		structLetDecl{visibility: "internal", letName: "percent", letType: []string{"Float"}},
		endOfStruct{},
		integerEnumDecl{visibility: "internal", name: "Planet", protocols: []string{"Codable"}, cases: []integerEnumCase{
			{name: "mercury", value: 0},
			{name: "venus", value: 1},
			{name: "earth", value: 100},
			{name: "mars", value: 101},
			{name: "jupiter", value: 102},
			{name: "saturn", value: 1000},
			{name: "uranus", value: 1001},
			{name: "neptune", value: 1002},
		}},
		stringEnumDecl{visibility: "public", name: "CompassPoint", protocols: []string{"Codable"},
			cases: []string{"north", "south", "east", "west"}},
	}
}

func Test_readInputSource(t *testing.T) {
	input := trimLeftBlock(`
		struct Order: Codable {
			let orderNo: String
			let lines: [OrderLine] //there must be at least one line
			let vouchers: [Character: Voucher]
			let planet: Planet
		}
		
		// This is an order line
		public struct OrderLine: Equatable, Codable {
			public let lineNo: Int32

		    // Article number is not set initially
			public let articleNo: Int64?
		}
		
		struct Voucher: Codable {
			let percent: Float
		}
		
		enum Planet: Int32, Codable {
			case mercury, venus, earth = 100, mars, jupiter, saturn = 1000, uranus, neptune
		}

		public enum CompassPoint: String, Codable {
			case north, south, east, west
		}`)

	actual := readInputSource(strings.NewReader(input), "mypackage")

	assertEqual(t, standardSyntaxUnitTest(), actual)
}

func Test_hydraType_validate(t *testing.T) {
	tests := []struct {
		name      string
		hydraType hydraType
		wantErr   bool
	}{
		{
			name:      "array with <>",
			hydraType: []string{"Array<", "Int32", ">"},
			wantErr:   true,
		},
		{
			name:      "array with []",
			hydraType: []string{"[", "Order_23", "]"},
			wantErr:   false,
		},
		{
			name:      "tuple",
			hydraType: []string{"(", "Int32", ",", "String", ")"},
			wantErr:   true,
		},
		{
			name:      "empty token",
			hydraType: []string{"Int32", ""},
			wantErr:   true,
		},
		{
			name:      "whitespace token",
			hydraType: []string{"Int32", " "},
			wantErr:   true,
		},
		{
			name:      "bad character",
			hydraType: []string{"Tvååker"},
			wantErr:   true,
		},
		{
			name:      "long valid type",
			hydraType: []string{"[", "Bool", "?", ":", "[", "[", "String", ":", "[", "Float", "]", "]", "]", "]"},
			wantErr:   false,
		},
		{
			name:      "map key cannot be array",
			hydraType: []string{"[", "[", "Bool", "]", ":", "String", "]"},
			wantErr:   true,
		},
		{
			name:      "map key cannot be map",
			hydraType: []string{"[", "[", "Int32", ":", "Bool", "]", ":", "String", "]"},
			wantErr:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hydraType.validate(); (err != nil) != tt.wantErr {
				t.Errorf("validateHydraType() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// ======= helpers =======

func assertEqual(t *testing.T, expected interface{}, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Fatalf("%v != %v", expected, actual)
	}
}

func trimLeftBlock(s string) string {
	lines := strings.Split(strings.Replace(s, "\t", "    ", -1), "\n")
	indexToTrimTo := math.MaxInt32

	for _, line := range lines {
		i := strings.IndexFunc(line, func(c rune) bool {
			return c != ' '
		})
		if i != -1 && i < indexToTrimTo {
			indexToTrimTo = i
		}
	}

	if indexToTrimTo == math.MaxInt32 {
		return strings.TrimSpace(s)
	}

	builder := strings.Builder{}
	for _, line := range lines {
		if strings.TrimSpace(line) == "" {
			builder.WriteRune('\n')
			continue
		}
		runes := []rune(line)
		newline := string(runes[indexToTrimTo:])
		builder.WriteString(newline)
		builder.WriteRune('\n')
	}

	return strings.TrimSpace(builder.String())
}

func toTabs(s string) string {
	return strings.Replace(s, "    ", "\t", -1)
}

func Test_isValidPackageName(t *testing.T) {
	tests := []struct {
		name        string
		packageName string
		want        bool
	}{
		{
			name:        "valid with dots",
			packageName: "hello.World",
			want:        true,
		},
		{
			name:        "invalid character",
			packageName: "hello@world",
			want:        false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidPackageName(tt.packageName); got != tt.want {
				t.Errorf("isValidPackageName() = %v, want %v", got, tt.want)
			}
		})
	}
}
