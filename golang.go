package main

import (
	"fmt"
	"io"
	"strconv"
	"strings"
	"unicode"
)

type golang struct {
	writer io.Writer
}

func (lang golang) writeOutput(sus []syntaxUnit) {

	panicWithError := func(err error, su syntaxUnit) {
		panic(fmt.Sprintf("error while writing '%v' as golang code: %v", su, err))
	}

	vis := getAllUserTypeVisibilities(sus)

	for i := 0; i < len(sus); i++ {
		su := sus[i]

		switch v := su.(type) {
		case startOfFile:
			if _, err := fmt.Fprint(lang.writer, "package ", golangPackage(v.packageName)); err != nil {
				panicWithError(err, su)
			}
		case startOfStruct:
			if _, err := fmt.Fprint(lang.writer, golangLinePrefix(sus[i-1]),
				"type ", golangVisibilityOf(v.name, v.visibility), " struct {"); err != nil {
				panicWithError(err, su)
			}
		case structLetDecl:
			structSU := getCurrentStructSU(i, sus)
			if _, err := fmt.Fprint(lang.writer, golangLinePrefix(sus[i-1]),
				"\t", golangVisibilityOf(v.letName, v.visibility), " ", formatGolangType(v.letType, vis),
				formatGolangStructTags(structSU.protocols, v.letName)); err != nil {
				panicWithError(err, su)
			}
		case endOfStruct:
			if _, err := fmt.Fprint(lang.writer, golangLinePrefix(sus[i-1]), "}"); err != nil {
				panicWithError(err, su)
			}
		case singleLineComment:
			if _, err := fmt.Fprint(lang.writer, golangLinePrefix(sus[i-1]),
				cntToTabIndent(v.indent), "//", v.comment); err != nil {
				panicWithError(err, su)
			}
		case endOfLineComment:
			if _, err := fmt.Fprint(lang.writer, " //", v.comment); err != nil {
				panicWithError(err, su)
			}
		case integerEnumDecl:
			if _, err := fmt.Fprint(lang.writer, golangLinePrefix(sus[i-1]),
				"type ", golangVisibilityOf(v.name, v.visibility), " int32\n\n",
				"const (\n\t", formatGolangIntegerEnumCases(v), "\n)"); err != nil {
				panicWithError(err, su)
			}
		case stringEnumDecl:
			if _, err := fmt.Fprint(lang.writer, golangLinePrefix(sus[i-1]),
				"type ", golangVisibilityOf(v.name, v.visibility), " string\n\n",
				"const (\n\t", formatGolangStringEnumCases(v), "\n)"); err != nil {
				panicWithError(err, su)
			}
		default:
			panic(fmt.Sprintf("unknown syntax unit: %v", su))
		}
	}
}

// a precondition is that hydra type is valid
func formatGolangType(hydraType hydraType, vis map[string]string) string {

	if len(hydraType) > 0 && hydraType[0] == "[" {
		p1, p2, rest := hydraType.extractMapOrArrayParts()
		rest = golangPopAnyStartingOptionType(rest)
		if len(p2) > 0 {
			return "map[" + formatGolangType(p1, vis) + "]" + formatGolangType(p2, vis) + formatGolangType(rest, vis)
		}

		return "[]" + formatGolangType(p1, vis) + formatGolangType(rest, vis)
	}

	hydraType = golangMoveOptionTypesOneLeft(hydraType)

	return strings.Join(mapS(hydraType, func(t string) string {
		switch t {
		case "Int8":
			return "int8"
		case "Int16":
			return "int16"
		case "Int32":
			return "int32"
		case "Int64":
			return "int64"
		case "Int":
			return "int"
		case "Character":
			return "rune"
		case "String":
			return "string"
		case "Bool":
			return "bool"
		case "Float":
			return "float32"
		case "Double":
			return "float64"
		case "[", ":", "]":
			panic(fmt.Sprintf("bad hydra type '%v' in %v", t, hydraType))
		default:
			if visibility, ok := vis[t]; ok {
				return golangVisibilityOf(t, visibility)
			}
			return t
		}
	}), "")
}

func golangPopAnyStartingOptionType(hydraType hydraType) hydraType {
	if len(hydraType) > 0 && hydraType[0] == "?" {
		return hydraType[1:]
	}

	return hydraType
}

func golangMoveOptionTypesOneLeft(hydraType hydraType) hydraType {
	for i, v := range hydraType {
		if i > 0 && v == "?" {
			hydraType[i] = hydraType[i-1]
			hydraType[i-1] = "*"
		}
	}

	return hydraType
}

func golangVisibilityOf(identifier string, hydraVisibility string) string {
	for _, r := range identifier {
		switch hydraVisibility {
		case "public":
			return string(unicode.ToUpper(r)) + identifier[1:]
		case "internal":
			return string(unicode.ToLower(r)) + identifier[1:]
		default:
			panic(fmt.Sprint("invalid visibility: ", hydraVisibility))
		}
	}

	panic(fmt.Sprintf("empty identifier!"))
}

func golangLinePrefix(lastSU syntaxUnit) string {
	switch lastSU.(type) {
	case startOfFile:
		return "\n\n"
	case startOfStruct:
		return "\n"
	case structLetDecl:
		return "\n"
	case endOfStruct:
		return "\n\n"
	case singleLineComment:
		return "\n"
	case endOfLineComment:
		return "\n"
	case integerEnumDecl:
		return "\n\n"
	case stringEnumDecl:
		return "\n\n"
	default:
		panic(fmt.Sprintf("unknown syntax unit: %v", lastSU))
	}
}

func formatGolangStructTags(protocols []string, fieldName string) string {
	for _, p := range protocols {
		if p == "Codable" {
			return fmt.Sprintf(" `json:\"%s\"`", fieldName)
		}
	}

	return ""
}

func golangPackage(packageName string) string {
	if len(packageName) == 0 {
		return "main"
	}

	return packageName
}

func formatGolangIntegerEnumCases(su integerEnumDecl) string {
	enumName := golangVisibilityOf(su.name, su.visibility)
	builder := strings.Builder{}
	for i, v := range su.cases {
		builder.WriteString(enumName)
		builder.WriteString("_")
		builder.WriteString(v.name)
		builder.WriteString(" = ")
		builder.WriteString(enumName)
		builder.WriteRune('(')
		builder.WriteString(strconv.Itoa(int(v.value)))
		builder.WriteRune(')')
		if i < len(su.cases)-1 {
			builder.WriteString("\n\t")
		}
	}
	return builder.String()
}

func formatGolangStringEnumCases(su stringEnumDecl) string {
	enumName := golangVisibilityOf(su.name, su.visibility)
	builder := strings.Builder{}
	for i, v := range su.cases {
		builder.WriteString(enumName)
		builder.WriteString("_")
		builder.WriteString(v)
		builder.WriteString(" = ")
		builder.WriteString(enumName)
		builder.WriteString("(\"")
		builder.WriteString(v)
		builder.WriteString("\")")
		if i < len(su.cases)-1 {
			builder.WriteString("\n\t")
		}
	}
	return builder.String()
}
