package main

import (
	"bytes"
	"strings"
	"testing"
)

func Test_golang_writeOutput(t *testing.T) {
	buf := &bytes.Buffer{}
	sut := golang{writer: buf}

	sut.writeOutput(standardSyntaxUnitTest())

	actual := strings.TrimSpace(buf.String())
	expected := toTabs(trimLeftBlock(strings.Replace(`
		package mypackage

		type order struct {
			orderNo string 'json:"orderNo"'
			lines []OrderLine 'json:"lines"' //there must be at least one line
			vouchers map[rune]voucher 'json:"vouchers"'
			planet planet 'json:"planet"'
		}
		
		// This is an order line
		type OrderLine struct {
			LineNo int32 'json:"lineNo"'
			// Article number is not set initially
			ArticleNo *int64 'json:"articleNo"'
		}
		
		type voucher struct {
        	percent float32 'json:"percent"'
		}
		
		type planet int32

		const (
			planet_mercury = planet(0)
			planet_venus = planet(1)
			planet_earth = planet(100)
			planet_mars = planet(101)
			planet_jupiter = planet(102)
			planet_saturn = planet(1000)
			planet_uranus = planet(1001)
			planet_neptune = planet(1002)
		)

		type CompassPoint string

		const (
			CompassPoint_north = CompassPoint("north")
			CompassPoint_south = CompassPoint("south")
			CompassPoint_east = CompassPoint("east")
			CompassPoint_west = CompassPoint("west")
		)
		`, "'", "`", -1)))
	assertEqual(t, expected, actual)
}

func Test_formatGolangType(t *testing.T) {
	tests := []struct {
		name      string
		hydraType hydraType
		sos       map[string]string
		want      string
	}{
		{
			name:      "array to list",
			hydraType: []string{"[", "Int32", "]"},
			sos:       map[string]string{},
			want:      "[]int32",
		},
		{
			name:      "2D array to 2D list",
			hydraType: []string{"[", "[", "String", "]", "]"},
			sos:       map[string]string{},
			want:      "[][]string",
		},
		{
			name:      "option type",
			hydraType: []string{"Int", "?"},
			sos:       map[string]string{},
			want:      "*int",
		},
		{
			name:      "nullable array",
			hydraType: []string{"[", "String", "]", "?"},
			sos:       map[string]string{},
			want:      "[]string",
		},
		{
			name:      "map type",
			hydraType: []string{"[", "Character", ":", "Double", "]"},
			sos:       map[string]string{},
			want:      "map[rune]float64",
		},
		{
			name:      "option type after map",
			hydraType: []string{"[", "Int8", ":", "Order", "?", "]", "?"},
			sos:       map[string]string{"Order": "internal"},
			want:      "map[int8]*order",
		},
		{
			name:      "map with array",
			hydraType: []string{"[", "Bool", ":", "[", "Float", "]", "]"},
			sos:       map[string]string{},
			want:      "map[bool][]float32",
		},
		{
			name:      "deep map-array combinations", // [Bool?: [[String : [Float]]]]
			hydraType: []string{"[", "Bool", "?", ":", "[", "[", "String", ":", "[", "Float", "]", "]", "]", "]"},
			sos:       map[string]string{},
			want:      "map[*bool][]map[string][]float32",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.hydraType.validate(); err != nil {
				t.Errorf("precondition failed for test '%v': %v", tt.name, err)
			}
			if got := formatGolangType(tt.hydraType, tt.sos); got != tt.want {
				t.Errorf("formatGolangType() = %v, want %v", got, tt.want)
			}
		})
	}
}
