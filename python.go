package main

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

type python struct {
	writer io.Writer
}

func (lang python) writeOutput(sus []syntaxUnit) {

	panicWithError := func(err error, su syntaxUnit) {
		panic(fmt.Sprintf("error while writing '%v' as python code: %v", su, err))
	}

	for i := 0; i < len(sus); i++ {
		su := sus[i]

		switch v := su.(type) {
		case startOfFile:
			if _, err := fmt.Fprint(lang.writer, "from __future__ import annotations\n",
				pythonImportDataclasses(sus), pythonImportEnum(sus), pythonImportTyping(sus)); err != nil {
				panicWithError(err, su)
			}
		case startOfStruct:
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(sus[i-1]), "@dataclass(frozen=True)\n",
				"class ", v.name, ":"); err != nil {
				panicWithError(err, su)
			}
		case structLetDecl:
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(sus[i-1]),
				"    ", v.letName, ": ", formatPythonType(v.letType)); err != nil {
				panicWithError(err, su)
			}
		case endOfStruct:
		case singleLineComment:
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(sus[i-1]),
				cntToSpaceIndent(v.indent), "#", v.comment); err != nil {
				panicWithError(err, su)
			}
		case endOfLineComment:
			if _, err := fmt.Fprint(lang.writer, " #", v.comment); err != nil {
				panicWithError(err, su)
			}
		case integerEnumDecl:
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(sus[i-1]),
				"class ", v.name, "(int, Enum):\n    ",
				formatPythonIntegerEnumCases(v.cases)); err != nil {
				panicWithError(err, su)
			}
		case stringEnumDecl:
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(sus[i-1]),
				"class ", v.name, "(str, Enum):\n    ", formatPythonStringEnumCases(v.cases)); err != nil {
				panicWithError(err, su)
			}
		default:
			panic(fmt.Sprintf("unknown syntax unit: %v", su))
		}
	}

	userTypes := getUserTypesDict(sus)
	lastSU := sus[len(sus)-1]

	// print dict_to_mytype functions
	for i := 0; i < len(sus); i++ {
		su := sus[i]

		switch v := su.(type) {
		case startOfStruct:
			if !v.isCodable() {
				continue
			}
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(lastSU),
				"def dict_to_", strings.ToLower(v.name), "(d: Dict[str, Any]) -> ", v.name,
				":\n    return ", v.name, "("); err != nil {
				panicWithError(err, su)
			}
		case structLetDecl:
			if !getCurrentStructSU(i, sus).isCodable() {
				continue
			}
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(lastSU),
				"        ", v.letName, "=", formatPythonDictExpr(v, userTypes), ","); err != nil {
				panicWithError(err, su)
			}
		case endOfStruct:
			if !getCurrentStructSU(i, sus).isCodable() {
				continue
			}
			if _, err := fmt.Fprint(lang.writer, pythonLinePrefix(lastSU), "    )"); err != nil {
				panicWithError(err, su)
			}
		default:
			continue
		}

		lastSU = su
	}
}

func formatPythonType(hydraType hydraType) string {

	if len(hydraType) > 0 && hydraType[0] == "[" {
		p1, p2, rest := hydraType.extractMapOrArrayParts()
		var t []string
		if len(p2) > 0 {
			t = []string{"Dict[" + formatPythonType(p1) + ", " + formatPythonType(p2) + "]"}
		} else {
			t = []string{"List[" + formatPythonType(p1) + "]"}
		}

		return formatPythonType(append(t, rest...))
	}

	if len(hydraType) > 0 && hydraType[len(hydraType)-1] == "?" {
		return "Optional[" + formatPythonType(hydraType[0:len(hydraType)-1]) + "]"
	}

	return strings.Join(mapS(hydraType, func(t string) string {
		switch t {
		case "Int8", "Int16", "Int32", "Int64", "Int":
			return "int"
		case "Character", "String":
			return "str"
		case "Bool":
			return "bool"
		case "Float", "Double":
			return "float"
		case "[", ":", "]":
			panic(fmt.Sprintf("bad hydra type '%v' in %v", t, hydraType))
		default:
			return t
		}
	}), "")
}

func pythonLinePrefix(lastSU syntaxUnit) string {
	switch lastSU.(type) {
	case startOfFile:
		return "\n\n\n"
	case startOfStruct:
		return "\n"
	case structLetDecl:
		return "\n"
	case endOfStruct:
		return "\n\n\n"
	case singleLineComment:
		return "\n"
	case endOfLineComment:
		return "\n"
	case integerEnumDecl:
		return "\n\n\n"
	case stringEnumDecl:
		return "\n\n\n"
	default:
		panic(fmt.Sprintf("unknown syntax unit: %v", lastSU))
	}
}

func formatPythonIntegerEnumCases(cases []integerEnumCase) string {
	builder := strings.Builder{}
	for i, v := range cases {
		builder.WriteString(v.name)
		builder.WriteString(" = ")
		builder.WriteString(strconv.Itoa(int(v.value)))
		if i < len(cases)-1 {
			builder.WriteString("\n    ")
		}
	}
	return builder.String()
}

func formatPythonStringEnumCases(cases []string) string {
	builder := strings.Builder{}
	for i, v := range cases {
		builder.WriteString(v)
		builder.WriteString(" = \"")
		builder.WriteString(v)
		builder.WriteRune('"')
		if i < len(cases)-1 {
			builder.WriteString("\n    ")
		}
	}
	return builder.String()
}

func pythonImportDataclasses(sus []syntaxUnit) string {
	for _, su := range sus {
		if _, ok := su.(startOfStruct); ok {
			return "from dataclasses import dataclass\n"
		}
	}

	return ""
}

func pythonImportEnum(sus []syntaxUnit) string {
	res := "from enum import Enum\n"
	for _, su := range sus {
		switch su.(type) {
		case integerEnumDecl:
			return res
		case stringEnumDecl:
			return res
		}
	}

	return ""
}

func pythonImportTyping(sus []syntaxUnit) string {
	types := allTypeDecls(sus)
	imports := []string{}

	if anyHT(types, func(ht hydraType) bool { return ht.containsOptional() }) {
		imports = append(imports, "Optional")
	}
	if anyHT(types, func(ht hydraType) bool { return ht.containsList() }) {
		imports = append(imports, "List")
	}

	hasAnyCodable := anyCodableStruct(sus)
	if hasAnyCodable || anyHT(types, func(ht hydraType) bool { return ht.containsDictionary() }) {
		imports = append(imports, "Dict")
	}
	if hasAnyCodable {
		imports = append(imports, "Any")
	}

	if len(imports) == 0 {
		return ""
	}

	return "from typing import " + strings.Join(imports, ", ")
}

func formatPythonDictExpr(letDecl structLetDecl, userTypes map[string]syntaxUnit) string {
	return formatPythonDictExprInternal(
		filter(letDecl.letType, func(s string) bool { return s != "?" }),
		fmt.Sprintf("d['%s']", letDecl.letName),
		userTypes,
	)
}

func formatPythonDictExprInternal(hydraType hydraType, varName string, userTypes map[string]syntaxUnit) string {

	if len(hydraType) > 0 && hydraType[0] == "[" {
		p1, p2, rest := hydraType.extractMapOrArrayParts()
		if len(rest) != 0 {
			panic(fmt.Sprintf("bad type: %v", strings.Join(hydraType, "")))
		}

		next := nextPythonVarName(varName)

		if len(p2) > 0 {
			nextNext := nextPythonVarName(next)
			return "{" + next + ": " + formatPythonDictExprInternal(p2, nextNext, userTypes) +
				" for " + next + ", " + nextNext + " in " + varName + ".items()}"
		}

		return "[" + formatPythonDictExprInternal(p1, next, userTypes) +
			" for " + next + " in " + varName + "]"
	}

	if len(hydraType) != 1 {
		panic(fmt.Sprintf("bad type: %v", strings.Join(hydraType, "")))
	}

	t := hydraType[0]
	if su, ok := userTypes[t]; ok {
		switch su.(type) {
		case startOfStruct:
			return fmt.Sprintf("dict_to_%s(%s)", strings.ToLower(t), varName)
		case integerEnumDecl, stringEnumDecl:
			return fmt.Sprintf("%s(%s)", t, varName)
		default:
			panic(fmt.Sprintf("unknown type: %v", su))
		}
	}

	return varName
}

func nextPythonVarName(varName string) string {
	if len(varName) > 1 {
		return "a"
	}
	if varName == "c" {
		return "e"
	}

	return string(rune(int(varName[0]) + 1))
}
