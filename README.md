# Hydra

A simple swift-based language for code sharing across multiple target languages
including Swift, Kotlin, C#, Python, Go, TypeScript/JavaScript.

Both the multitranspiler and the subset-swift language are called Hydra.
The .swift file extension is kept for better IDE-support.

The first version will be limited to data-only structs,
which for instance can be used to share DTO models.

## How to run

To generate code, use std-in and std-out, usually as part of some pre-build step.
``` bash
hydra -l kotlin < DTOs.swift > DTOs.kt
```

You always can run/validate it as a standalone swift script:
``` bash
swift DTOs.swift
```

Or maybe check that it is a valid Hydra file first it by:
``` bash
swift <(hydra < DTOs.swift)
```

To make the last command always emit an error code on failure, do:
``` bash
swift <(hydra < DTOs.swift 2>&1)
```
This can be useful to validate the code as part of a CI step.

## Transpilation Examples

The following Hydra/Swift code
``` swift
struct Order {
    let orderNo: String
    let lines: [OrderLine]
}

struct OrderLine {
    let lineNo: Int32
    let articleNo: Int64?
}
```

gives in Kotlin
``` kotlin
internal data class Order(
    internal val orderNo: String,
    internal val lines: List<OrderLine>
)

internal data class OrderLine(
    internal val lineNo: Int,
    internal val articleNo: Long?
)
```

or in Go
``` go
package main

type order struct {
    orderNo string
    lines []orderLine
}

type orderLine struct {
    lineNo int32
    articleNo *int64
}
```

or in Python
``` python
@dataclass(frozen=True)
class Order:
    orderNo: str
    lines: List[OrderLine]


@dataclass(frozen=True)
class OrderLine:
    lineNo: int
    articleNo: Optional[int]
```

## Built-in types

range | Hydra/Swift | Kotlin | Go | Python
--- | --- | --- | --- | ---
[-128, 127] | Int8 | Byte | int8 | int
[-32768, 32767] | Int16 | Short | int16 | int
[-2^31, 2^31 - 1] | Int32 | Int | int32 | int
[-2^63, 2^63 - 1] | Int64 | Long | int64 | int
32 or 64 bit | Int | Long | int | int
unicode point | Character | Char | rune | str
- | String | String | string | str
[0, 1] | Bool | Boolean | bool | bool
IEEE-754 32-bit | Float | Float | float32 | float
IEEE-754 64-bit | Double | Double | float64 | float
- | [T] | List<T> | []T | List[T]
- | [K: V] | Map<K, V> | map[K]V | Dict[K, V]
- | T? | T? | *T | Optional[T]

## Visibility levels

Hydra/Swift | Kotlin | Go | Python
--- | --- | --- | ---
internal (default) | internal | first letter lowercase | no "_" prefix
public | public (default) | first letter uppercase | no "_" prefix

## Structs

`struct`s are, together with enums, the only types the user can define.
 
 * They cannot be empty, there must be at least one field.
 * They cannot have methods, only data.
 * They are immutable, only `let` are allowed.

Structs can extend empty protocols or protocols with a default implementation like `Equatable`.
The following protocols affects the transpilated output:

Hydra/Swift | Kotlin | Go | Python
--- | --- | --- | ---
Codable | @Serializable | Json Struct Tags | generates a `dict_to_mytype()`

Valid characters in all identifiers are `a-z`, `A-Z`, `0-9` and `_`, must begin with non-numeric.

## Enums

`enum`s are named integer- or string constants. The only accepted formats are these:

``` swift
enum Planet: Int32 {
    case mercury, venus, earth = 100, mars, jupiter, saturn = 1000, uranus, neptune
}

enum CompassPoint: String {
    case north, south, east, west
}
```

 * Integer enum values can be overridden, same rules as in [swift](https://docs.swift.org/swift-book/LanguageGuide/Enumerations.html#ID149)
 * String enum values are always the same as the identifier name
 * Can extend empty protocols or protocols with a default implementation like structs
 * `Codable` make them json serializable in target language, same as structs

## Comments

``` swift
// This is a line comment
struct Hello {
    let world: String // also works here
}
```

## Package/namespace/module name

The output source file must usually be contained in some kind of package/namespace/module.
These values are used per default, but they can be overridden by the `-p` flag.

Language | Default | Can be overridden
--- | --- | ---
Hydra/Swift | Doesn't use file namespacing | No
Kotlin | Empty package name | Yes
Go | `main` | Yes
Python | Filename is modulename | No

