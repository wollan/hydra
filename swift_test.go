package main

import (
	"bytes"
	"strings"
	"testing"
)

func Test_swift_writeOutput(t *testing.T) {
	buf := &bytes.Buffer{}
	sut := swift{writer: buf}

	sut.writeOutput(standardSyntaxUnitTest())

	actual := strings.TrimSpace(buf.String())
	expected := trimLeftBlock(`
		struct Order: Codable {
			let orderNo: String
			let lines: [OrderLine] //there must be at least one line
			let vouchers: [Character: Voucher]
			let planet: Planet
		}
		
		// This is an order line
		public struct OrderLine: Equatable, Codable {
			public let lineNo: Int32
			// Article number is not set initially
			public let articleNo: Int64?
		}
		
		struct Voucher: Codable {
			let percent: Float
		}

        enum Planet: Int32, Codable {
            case mercury = 0,
                 venus = 1,
                 earth = 100,
                 mars = 101,
                 jupiter = 102,
                 saturn = 1000,
                 uranus = 1001,
                 neptune = 1002
        }

		public enum CompassPoint: String, Codable {
			case north, south, east, west
		}`)

	assertEqual(t, expected, actual)
}
