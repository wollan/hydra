#!/bin/bash -ex

cd $(dirname $0)

go build
go test

# integration tests
cd integration-tests
languages=(swift kotlin python)
extensions=(swift kt py)
for i in "${!languages[@]}"; do
    l="${languages[$i]}"
    ext="${extensions[$i]}"
    ../hydra -l $l < hydra.swift > $l/sut.$ext
    cp in.json $l/
    docker build -f $l/Dockerfile -t hydra-tests-$l $l
    docker run --rm hydra-tests-$l > out.json
    go run jsonassert.go in.json out.json
done
../hydra -l go < hydra.swift > go/sut.go
go run go/main.go go/sut.go > out.json
go run jsonassert.go in.json out.json
